;;; To use, put in .emacs:
;;; (load "~/.config/brlewis-config/emacs/startup")

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
(when (< emacs-major-version 24)
  (add-to-list 'package-archives '("gnu" . "https://elpa.gnu.org/packages/")))
(package-initialize)

(when (not (package-installed-p 'use-package))
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package-ensure)
(setq use-package-always-ensure t)

(server-start)

(setq inhibit-startup-screen t)

(setq-default indent-tabs-mode nil)
(autoload
  'whitespace-mode
  "whitespace"
  "Toggle whitespace visualization."
  t)

(use-package bind-key
  :config
  (bind-key "M-=" 'text-scale-increase)
  (bind-key "M--" 'text-scale-decrease))
(use-package magit)
(use-package company
  :hook ((js-mode . company-mode)
         (typescript-mode . company-mode)
         (typescript-ts-mode . company-mode)
         (tsx-ts-mode . company-mode))
  :custom
  ;; aligns annotation to the right hand side
  (company-tooltip-align-annotations t))
(use-package treesit-auto
  :custom
  (treesit-auto-install 'prompt)
  :config
  (treesit-auto-add-to-auto-mode-alist 'all)
  (global-treesit-auto-mode))
(use-package exec-path-from-shell ; Really only for OSX
  :if (memq window-system '(mac ns))
  :config (exec-path-from-shell-initialize))
(use-package markdown-mode)
(use-package real-auto-save)            ; Enable manually within Dropbox
(use-package add-node-modules-path
  :hook ((js-mode . add-node-modules-path)
         (typescript-mode . add-node-modules-path)
         (typescript-ts-mode . add-node-modules-path)
         (tsx-ts-mode . add-node-modules-path)
         (css-mode . add-node-modules-path)))
(defun prettier-allow ()
  (interactive)
  (when (executable-find "prettier")
    (prettier-js-mode)))
(use-package prettier-js
  :hook ((js-ts-mode . prettier-allow)
         (typescript-ts-mode . prettier-allow)
         (css-mode . prettier-allow))
  :custom
  (css-indent-offset 2))
(defun maybe-eglot-format ()
  (interactive)
  (when eglot--managed-mode
    (eglot-format-buffer)))
(use-package eglot
  :hook ((before-save . maybe-eglot-format)
         (js-ts-mode . eglot-ensure)
         (markdown-mode . eglot-ensure)
         (tsx-ts-mode . eglot-ensure)
         (typescript-ts-mode . eglot-ensure))
  :custom
  (js-indent-level 2)
  (typescript-indent-level 2)
  (tab-width 2)
  (markdown-unordered-list-item-prefix "- ") ; to match deno
  :config
  (add-to-list 'project-vc-extra-root-markers "deno.json")
  (add-to-list
   'eglot-server-programs
   '(markdown-mode . ("deno" "lsp")))
  (add-to-list
   'eglot-server-programs
   `((js-mode js-ts-mode tsx-ts-mode
              typescript-ts-mode typescript-mode) .
              ,(eglot-alternatives
                '(("typescript-language-server" "--stdio")
                  ("deno" "lsp"))))))

(load-theme 'wombat)
(desktop-save-mode 1)
