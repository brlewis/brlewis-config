brlewisInstall () {
    brew cask install emacs
    brew install ispell nvm
    grep -q brlewis-config ~/.bashrc || echo '. $HOME/.config/brlewis-config/bash/ubuntu.sh' >> ~/.bashrc
    grep -q brlewis-config ~/.emacs || echo '(load "~/.config/brlewis-config/emacs/startup")' >> ~/.emacs
}

jiramsg () {
    cp -pi ~/.config/brlewis-config/bash/prepare-commit-msg .git/hooks/
}

export NVM_DIR="$HOME/.nvm"
. "/usr/local/opt/nvm/nvm.sh"

. $HOME/.config/brlewis-config/bash/common.sh
