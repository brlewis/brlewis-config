# . $HOME/.config/brlewis-config/bash/ubuntu.sh

certbotInstall () {
    sudo mkdir -p /opt /etc/letsencrypt
    sudo chown $LOGNAME /opt /etc/letsencrypt
    git clone https://github.com/certbot/certbot /opt/letsencrypt
    mkdir -p /opt/$LOGNAME/
}

nvmInstall () {
    wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.39.7/install.sh | bash
}

yarnInstall () {
    wget -q -O - https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
    sudo apt-get update && sudo apt-get install yarn
}

vscodeInstall () {
    wget -q -O - https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
    sudo mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg
    sudo sh -c 'echo "deb https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
    sudo apt-get update
    sudo apt-get install code
}

brlewisGitlab() {
    ssh-keygen -t ed25519 -C $(hostname)
    sudo apt-get install xclip
    xclip -sel clip < ~/.ssh/id_ed25519.pub
    firefox https://gitlab.com/-/profile/keys
    echo 'TODO manually:'
    echo 'git config --global user.name "Your Name"'
    echo 'git config --global user.email "your_email_address@example.com"'
}

brlewisDropbox() {
    (cd ~ && wget -O - "https://www.dropbox.com/download?plat=lnx.x86_64" | tar xzf -)
}

brlewisInstall () {
    nvmInstall
    grep -q brlewis-config ~/.bashrc || echo '. $HOME/.config/brlewis-config/bash/ubuntu.sh' >> ~/.bashrc
    touch ~/.emacs
    grep -q brlewis-config ~/.emacs || echo '(load "~/.config/brlewis-config/emacs/startup")' >> ~/.emacs
    gsettings set org.gnome.desktop.interface show-battery-percentage true
    cp -i $HOME/.config/brlewis-config/bash/dotfiles/ubuntu/.??* ~
    sudo apt install mosh tlp ncal jq
    snap install emacs || sudo apt install emacs
    snap install deno || (wget -qO- https://deno.land/install.sh | bash)
}

brlewisEmacsFromSnap () {
    sudo update-alternatives --install /usr/bin/emacs emacs /snap/bin/emacs 50
}

export EDITOR=emacsclient

. $HOME/.config/brlewis-config/bash/common.sh
