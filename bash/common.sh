hostColor () {
    expr 1 + $(hostname | sum | awk '{print $1}') % 6
}

export PS1='\[\033[01;32m\]\w\[\033[33m\] $(git symbolic-ref HEAD --short 2> /dev/null) \[\033[00m\]$(date +%T)\n\[\033[01;32m\]\[\033[0m\033[0;32m\]\[\033[01;32m\]\u\[\033[3$(hostColor)m\]@\h\[\033[00m\]\$ '

still () {
    # Usage: still myvideo.mp4 30.50
    # to get a still frame from second 30.5 from myvideo.mp4
    # and save to myvideo.30.50.png
    local outfile=$(basename "$1" .mp4).$2.png
    ffmpeg -i "$1" -ss $2 -vframes 1 "$outfile" && xdg-open "$outfile"
}
