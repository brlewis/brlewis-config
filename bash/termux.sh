# . $HOME/.config/brlewis-config/bash/termux.sh

brlewisInstall () {
    pkg install emacs nodejs openssh
    npm install -g typescript
    grep -q brlewis-config ~/.bashrc || echo '. $HOME/.config/brlewis-config/bash/termux.sh' >> ~/.bashrc
    grep -q brlewis-config ~/.emacs || echo '(load "~/.config/brlewis-config/emacs/startup")' >> ~/.emacs
}

. $HOME/.config/brlewis-config/bash/common.sh
